<?php

/**
 * @file
 * Handles installing, uninstalling, and updating QBMS settings.
 */

/**
 * Implements hook_requirements().
 */
function uc_qbms_requirements($phase) {
  $t = get_t();

  // Check that the PHP CURL extension is loaded
  $has_curl = extension_loaded('curl');

  $requirements['uc_qbms_curl'] = array(
    'title' => $t('QBMS: cURL'),
    'value' => $has_curl ? $t('Installed') : $t('Not Installed'),
  );

  if (!$has_curl) {
    $requirements['uc_qbms_curl']['severity'] = REQUIREMENT_ERROR;
    $requirements['uc_qbms_curl']['description'] = $t("The Quickbooks Merchant Services API requires the PHP <a href='!curl_url'>CURL</a> library.", array('!curl_url' => 'http://www.php.net/curl'));
  }

  return $requirements;
}

/**
 * Implements hook_schema().
 */
function uc_qbms_schema() {
  $schema = array();

  $schema['uc_qbms_wallet'] = array(
    'description' => 'Stores the WalletID Indexed by CustomerID.',
    'fields' => array(
      'cid' => array(
        'description' => 'The CustomerID.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
      ),
      'wid' => array(
        'description' => 'The WalletID.',
        'type' => 'varchar',
        'length' => 40,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('cid'),
  );

  $schema['uc_qbms_trans'] = array(
    'description' => 'Stores the serialized Transaction object for later retrieval. Indexed by WalletID. No CC numbers are stored here.',
    'fields' => array(
      'tid' => array(
        'description' => 'The CreditCardTransID.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
      ),
      'trans' => array(
        'description' => 'The serialized Transaction object.',
        'type' => 'text',
        'size' => 'normal',
        'not null' => TRUE,
      ),
      'oid' => array(
        'description' => 'The OrderID.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
      ),
      'wid' => array(
        'description' => 'The WalletID.',
        'type' => 'varchar',
        'length' => 40,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('oid'),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function uc_qbms_uninstall() {
  // Delete settings variables .
  variable_del('uc_qbms_qb_php_dk_path');
  variable_del('uc_qbms_connection_ticket');
  variable_del('uc_qbms_txn_mode');
}

/**
 * Add wid, oid fields to {uc_qbms_trans} table.
 * Set oid as new primary key.
 */
function uc_qbms_update_7200() {
  $ret = array();
  $spec_wid = array(
    'description' => 'The WalletID.',
    'type' => 'varchar',
    'length' => 40,
    'not null' => TRUE,
  );
  db_add_field('uc_qbms_trans', 'wid', $spec_wid);
  $spec_oid = array(
    'description' => 'The OrderID.',
    'type' => 'varchar',
    'length' => 20,
    'not null' => TRUE,
  );
  db_add_field('uc_qbms_trans', 'oid', $spec_oid);
  db_drop_primary_key('uc_qbms_trans');
  // hook_update_N() no longer returns a $ret array. Instead, return
  // nothing or a translated string indicating the update ran successfully.
  // See http://drupal.org/node/224333#update_sql.
  return t('Update was successful.');
}

/**
 * Create {uc_qbms_wallet} table.
 */
function uc_qbms_update_7201() {
  $schema['uc_qbms_wallet'] = array(
    'description' => 'Stores the WalletID Indexed by CustomerID.',
    'fields' => array(
      'cid' => array(
        'description' => 'The CustomerID.',
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
      ),
      'wid' => array(
        'description' => 'The WalletID.',
        'type' => 'varchar',
        'length' => 40,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('cid'),
  );

  db_create_table('uc_qbms_wallet', $schema['uc_qbms_wallet']);
  // hook_update_N() no longer returns a $ret array. Instead, return
  // nothing or a translated string indicating the update ran successfully.
  // See http://drupal.org/node/224333#update_sql.
  return t('Update was successful.') /* $ret */;
}
