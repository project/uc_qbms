<?php

/**
 * @file
 * Process payments using Quickbooks Merchant Services (QBMS) for Web Store.
 */

/**
 * Default path to the Quickbooks PHP DevKit directory.
 */
define('UC_QBMS_PHP_DK_PATH', 'sites/all/libraries/quickbooks-php-master');

/**
 * Intuit's Live and Test Application Ids.
 *
 * You may also register your own Application Id's with Intuit and use that
 * AppLogin instead of the ones provided here.
 */
define('UC_QBMS_APPLOGIN', 'qbms.ubercart.org');
define('UC_QBMS_TEST_APPLOGIN', 'qbmstest.ubercart.org');

/**
 * Implements hook_payment_gateway().
 */
function uc_qbms_uc_payment_gateway() {
  $gateways[] = array(
    'id' => 'qbms',
    'title' => t('Quickbooks Payments'),
    'description' => t('Process credit card payments through Quickbooks Payments (formerly QBMS for Web Store).'),
    'settings' => 'uc_qbms_settings_form',
    'credit' => 'uc_qbms_card_process',
    'credit_txn_types' => array(UC_CREDIT_AUTH_CAPTURE),
  );

  return $gateways;
}

/**
 * Callback for payment gateway settings.
 */
function uc_qbms_settings_form() {
  // Verify that the QBMS PHP SDK is installed.
  $UC_QBMS_PHP_DK_PATH = variable_get('uc_qbms_php_dk_path', UC_QBMS_PHP_DK_PATH);
  if (!is_dir($UC_QBMS_PHP_DK_PATH)) {
    drupal_set_message(t('You need to download the <a href="@qb_php_dk">Quickbooks PHP DevKit</a> and extract the entire contents of the archive into the sites/all/libraries folder of your site so that the main library file is located here: @path. ', array('@qb_php_dk' => 'https://github.com/consolibyte/quickbooks-php', '@path' => 'sites/all/libraries/quickbooks-php-master/QuickBooks.php')), 'error');
  }
  $form['uc_qbms_php_dk_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to Quickbooks PHP DevKit'),
    '#default_value' => $UC_QBMS_PHP_DK_PATH,
    '#description' => t('Enter the path relative to Drupal root where the Quickbooks PHP DevKit directory is located. NO trailing slash!'),
  );

  // Retrieve the un-encrypted connection ticket.
  $login_data = uc_qbms_login_data();
  $form['uc_qbms_connection_ticket'] = array(
    '#type' => 'textfield',
    '#title' => t('Connection Ticket'),
    '#default_value' => $login_data['connection_ticket'],
    '#description' => t('Enter the Contection Ticket provided by Intuit.<br />Get your connection ticket from Intuit by connecting your QBMS account with this module at: <a href="@qb_appr">Quickbooks Merchant Service Center</a>', array('@qb_appr' => 'https://merchantaccount.quickbooks.com/j/sdkconnection?appid=179399124&appdata=mydata')),
  );

  $form['env'] = array(
    '#type' => 'fieldset',
    '#title' => t('Quickboos Payments Environment'),
  );
  $form['env']['uc_qbms_txn_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Transaction Mode'),
    '#default_value' => variable_get('uc_qbms_txn_mode', 'test'),
    '#options' => array(
      'live' => t('Live transactions'),
      'test' => t('Test transactions'),
    ),
    '#description' => t('Transactions are set to be used with an Intuit QBMS development account by default. Adjust to live transactions when you are ready to start processing real payments through a Quickbooks Payments production account.'),
  );

  return $form;
}

/**
 * Implements hook_form_FORM_ID_alter() for uc_payment_method_settings_form().
 */
function uc_qbms_form_uc_payment_method_settings_form_alter(&$form, &$form_state) {
  if ($form_state['build_info']['args'][0] == 'credit') {
    $form['#submit'][] = 'uc_qbms_payment_gateway_settings_submit';
  }
}

/**
 * Submit handler for payment gateway settings form to encrypt fields.
 */
function uc_qbms_payment_gateway_settings_submit($form, &$form_state) {
  // If CC encryption has been configured properly.
  if ($key = uc_credit_encryption_key()) {
    // Setup our encryption object.
    $crypt = new UbercartEncryption();

    // Encrypt the Connection Ticket.
    if (!empty($form_state['values']['uc_qbms_connection_ticket'])) {
      variable_set('uc_qbms_connection_ticket', $crypt->encrypt($key, $form_state['values']['uc_qbms_connection_ticket']));
    }

    // Store any errors.
    uc_store_encryption_errors($crypt, 'uc_qbms');
  }
}

/**
 * Handler for processing credit card transactions.
 *
 * @todo Implement additional methods supported by the QB PHP DevKit.
 */
function uc_qbms_card_process($order_id, $amount, $data) {
  // Load the order.
  $order = uc_order_load($order_id);

  // Perform the appropriate action based on the transaction type.
  switch ($data['txn_type']) {
    case UC_CREDIT_AUTH_CAPTURE:
    default:
      return uc_qbms_charge($order, $amount);
  }
}

/**
 * Handles UC_CREDIT_AUTH_CAPTURE transactions through QBMS for Web Store.
 */
function uc_qbms_charge($order, $amount) {
  // Require the QB PHP DevKit.
  require_once DRUPAL_ROOT . '/' . variable_get('uc_qbms_php_dk_path', UC_QBMS_PHP_DK_PATH) . '/' . 'QuickBooks.php';
  global $user;

  // Retrieve the un-encrypted connection ticket.
  $login_data = uc_qbms_login_data();
  $connection_ticket = $login_data['connection_ticket'];

  // Retrieve the appropriate AppLogin.
  if (variable_get('uc_qbms_txn_mode', 'test') == 'live') {
    $app_login = UC_QBMS_APPLOGIN;
    $test = FALSE;
  }
  else {
    $app_login = UC_QBMS_TEST_APPLOGIN;
    $test = TRUE;
  }
  $customerID = $order->uid;
  // Create an instance of the MerchantService object.
  $qbms = new QuickBooks_MerchantService(NULL, NULL, $app_login, $connection_ticket);
  // Set the QBMS test environment variable.
  $qbms->useTestEnvironment($test);

  if (strlen($order->payment_details['cc_number']) > 4 ) {
    // Create the CreditCard object.
    $card = new QuickBooks_MerchantService_CreditCard(
      $order->payment_details['cc_owner'],
      $order->payment_details['cc_number'],
      $order->payment_details['cc_exp_year'],
      $order->payment_details['cc_exp_month'],
      $order->billing_street1,
      $order->billing_postal_code,
      $order->payment_details['cc_cvv']
    );

    // Add this credit card to the wallet
    $walletID = $qbms->addWallet($customerID, $card);

    $wallet_query = db_query("SELECT wid from {uc_qbms_wallet} WHERE wid = :wid", array(':wid' => $walletID));
    $wallet_exists = $wallet_query->fetchObject;
    if (!$wallet_exists) {
      // Write the walletID to database.
      $wallet = new stdClass();
      $wallet->cid = $customerID;
      $wallet->wid = $walletID;
      drupal_write_record('uc_qbms_wallet', $wallet);
      watchdog('uc_qbms', 'Added walletID to database with customerID %id.', array('%id' => $wallet->cid));
    }
  }
  else { // UC_Recurring Charge
    // Get WalletID from database based on OrderID
    $wallet_query = db_query("SELECT wid from {uc_qbms_trans} WHERE oid = :oid", array(':oid' => $order->data['old_order_id']));
    $wid      = $wallet_query->fetchAssoc();
    $walletID = $wid['wid'];
  }

  if ($walletID) {
    // Formating for $amount variable, used later.
    $context = array(
      'revision' => 'formatted-original',
      'type' => 'amount',
    );

    // The transaction object only contains data if the submission was approved.
    if ($transaction = $qbms->chargeWallet($customerID, $walletID, $amount, NULL, NULL, $order->payment_details['cc_cvv'])) {
      //charge($card, $amount)) {
      // Convert transaction object into array.
      $transaction = $transaction->toArray();

      // Write the transaction to database.
      $record = new stdClass();
      $record->tid = $transaction['CreditCardTransID'];
      $record->trans = serialize($transaction);
      $record->oid = $order->order_id;
      $record->wid = $walletID;
      drupal_write_record('uc_qbms_trans', $record);
      watchdog('uc_qbms', 'Added transaction to database with id %id.', array('%id' => $record->tid));

      // Build a message for display and comments in the payments table.
      $message = t('Type: @type<br />CCTransID: @id', array('@type' => uc_qbms_txn_type($transaction['Type']), '@id' => $transaction['CreditCardTransID']));

      // Build $result array to be returned to uc_credit module
      $result = array(
        'success' => TRUE,
        'comment' => $message,
        'message' => $message,
        'data' => array(
          'module' => 'uc_qbms',
          'txn_type' => $transaction['Type'],
          'txn_id' => $transaction['CreditCardTransID'],
        ),
        'uid' => $user->uid,
      );

      // Build a comment for display in the Order notes.
      $comment = t('<b>@type</b><br /><b>@status:</b> @message<br />Amount: @amount<br />AVS Street response: @avss<br />AVS Zip response: @avsz',
        array(
        '@type' => $transaction['Type'],
        '@status' => $result['success'] ? t('ACCEPTED') : t('REJECTED'),
        '@message' => $transaction['PaymentStatus'],
        '@amount' => $amount,
        '@avss' => $transaction['AVSStreet'],
        '@avsz' => $transaction['AVSZip'],
      )
      );

      // Add the CVV response if enabled.
      if (variable_get('uc_credit_cvv_enabled', TRUE)) {
        $comment .= '<br />' . t('CVV match: @cvv', array('@cvv' => $transaction['CardSecurityCodeMatch']));
      }

    }
    // The transaction was not approved.
    else {
      // Get error code and message and store in an array.
      $error = array(
        'number' => $qbms->errorNumber(),
        'message' => $qbms->errorMessage(),
      );

      // Write errors to Watchdog
      watchdog('uc_qbms', 'QBMS error during charge: @error_num: @error', array('@error_num' => $error['number'], '@error' => $error['message']), WATCHDOG_ERROR);

      // Build $result array to be returned to uc_credit module
      $result = array(
        'success' => FALSE,
        'message' => t('Credit card payment declined: @message', array('@message' => $error['message'])),
        'uid' => $user->uid,
      );

      // Build a comment for display in the Order notes.
      $comment = t('<b>@status:</b> @message<br />Amount: @amount', array('@status' => $result['success'] ? t('ACCEPTED') : t('REJECTED'), '@message' => $error['message'], '@amount' => uc_price($amount, $context)));
    }

    // Save the comment to the order.
    uc_order_comment_save($order->order_id, $user->uid, $comment, 'admin');
  }
  else {
    $result['success'] = FALSE;
  }

  return $result;
}

/**
 * Returns the title of the transaction type.
 */
function uc_qbms_txn_type($type) {
  switch ($type) {
    case 'Charge':
      return t('Authorization and capture');
  }
}

/**
 * Decrypts the connection ticket for using QBMS APIs.
 */
function uc_qbms_login_data() {
  static $_uc_qbms_data;

  if (!empty($_uc_qbms_data)) {
    return $_uc_qbms_data;
  }

  $connection_ticket = variable_get('uc_qbms_connection_ticket', '');

  // If CC encryption has been configured properly.
  if ($key = uc_credit_encryption_key()) {
    // Setup our encryption object.
    $crypt = new UbercartEncryption();

    // Decrypt the connection_ticket.
    if (!empty($connection_ticket)) {
      $connection_ticket = $crypt->decrypt($key, $connection_ticket);
    }

    // Store any errors.
    uc_store_encryption_errors($crypt, 'uc_qbms');
  }

  $_uc_qbms_data = array(
    'connection_ticket' => $connection_ticket,
  );

  return $_uc_qbms_data;
}

/*
 ****** RECURRING PAYMENTS INTEGRATION *****
 */

/**
 * Implements hook_recurring_info().
 */
function uc_qbms_recurring_info() {
  $items['qbms'] = array(
    'name' => t('QBMS Gateway'),
    'payment method' => 'credit',
    'module' => 'uc_qbms',
    'fee handler' => 'qbms',
    'process callback' => 'uc_qbms_recurring_process',
    'renew callback' => 'uc_qbms_recurring_renew',
    'cancel callback' => 'uc_qbms_recurring_cancel',
    'saved profile' => TRUE,
    'menu' => array(
      'charge' => UC_RECURRING_MENU_DEFAULT,
      'edit' => UC_RECURRING_MENU_DEFAULT,
      'cancel' => UC_RECURRING_MENU_DEFAULT,
    ), // Use the default user operation defined in uc_recurring.
  );
  return $items;
}

/**
 * Callback for setting up a recurring fee.
 *
 * @param $order
 *   The order object containing billing and shipping information.
 * @param $fee
 *   An array of data describing the recurring fee.
 * @return
 *   TRUE or FALSE indicating the success of the request.
 */
function uc_qbms_recurring_process($order, &$fee) {
  return TRUE;
}

/**
 * Implemenation of hook_recurring_renew()
 *
 * Charges order WalletID for fee.
 */
function uc_qbms_recurring_renew($order, $fee) {
  $result = uc_qbms_charge($order, $fee->fee_amount);

  // Transaction succeeded.
  if ($result['success'] == TRUE) {
    // Build a message for display and comments in the payments table.
    $message = t('!amount recurring fee collected for @model. (ID: <a href="!url">!fee</a>)',
                  array(
      '!url' => url('admin/store/orders/recurring/view/fee/' . $fee->rfid),
      '!fee' => $fee->rfid,
      '!amount' => uc_currency_format($fee->fee_amount),
      '@model' => $fee->data['subscription_id'],
    ));
    // enter the payment.
    uc_payment_enter($order->order_id, 'credit', $order->order_total, 0, $result['data'], $message);

  }

  return $result['success'];
}

/**
 * Cancels an recurring subscription.
 *
 * @param $order_id
 *   The ID of the order the recurring fee was attached to.
 * @param $fee
 *   The data array for the recurring fee being canceled.
 * @return
 *   TRUE or FALSE indicating the success of the cancellation.
 */
function uc_qbms_cancel($fee) {
  return TRUE;
}
